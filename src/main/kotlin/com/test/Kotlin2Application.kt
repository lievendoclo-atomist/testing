package com.test

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Kotlin2Application

fun main(args: Array<String>) {
    runApplication<Kotlin2Application>(*args)
}
